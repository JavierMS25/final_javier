<?php

namespace App\Controllers;

use Config\Services;
use App\models\FamiliaModel;


class Home extends BaseController
{
    protected $auth;
    protected $session;
    
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        parent::initController($request, $response, $logger);
        //------------------------------------------------------------
        // Preload any models, libraries, etc, here.
        //------------------------------------------------------------
        $this->session = Services::session();
        $this->auth = new \IonAuth\Libraries\IonAuth();

    }


    public function index()
    {
        if (!$this->auth->loggedIn()){
            return redirect()->to('auth/login');
        }
        return view('welcome_message');
    }
    
    public function catalogo()
    {
        $familia = new FamiliaModel();

        $data['familias'] = $familia->findAll();

        print_r($data);
    }
}


    